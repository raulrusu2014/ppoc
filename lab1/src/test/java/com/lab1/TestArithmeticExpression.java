package com.lab1;

import com.tora.Model.Expression.Arithmetic.AdditionExpression;
import com.tora.Model.Expression.ExpressionFactory;
import com.tora.Model.Expression.IExpression;
import com.tora.Model.Expression.Simple.IntegerExpression;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class TestArithmeticExpression {
    private ExpressionFactory expressionFactory;

    @Before
    public void setup() {
        expressionFactory = new ExpressionFactory();
    }

    @Test
    public void testFactory() {
        var addExpression = expressionFactory.createSimpleAddition(1, 1);
        var subtractionExpression = expressionFactory.createSimpleSubtraction(1, 1);
        Assert.assertNotNull(addExpression);
        Assert.assertNotNull(subtractionExpression);
    }

    @Test
    public void simpleAdditionExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createSimpleAddition(3, 4);
        Assert.assertEquals(7, additionExpression.evaluate().intValue());
    }

    @Test
    public void nestedAdditionExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createAdditionExpression(expressionFactory.createSimpleAddition(1,1), expressionFactory.createSimpleAddition(1,1));
        Assert.assertEquals(4, additionExpression.evaluate().intValue());
    }

    @Test
    public void simpleSubtractionExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createSimpleSubtraction(4, 4);
        Assert.assertEquals(0, additionExpression.evaluate().intValue());
    }

    @Test
    public void nestedSubtractionExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createAdditionExpression(expressionFactory.createSimpleSubtraction(4,2), expressionFactory.createSimpleSubtraction(4,2));
        Assert.assertEquals(4, additionExpression.evaluate().intValue());
    }

    @Test
    public void simpleMultiplicationExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createSimpleMultiplication(4, 3);
        Assert.assertEquals(12, additionExpression.evaluate().intValue());
    }

    @Test
    public void simpleDivisionExpressionEvaluatesCorrectly() {
        var additionExpression = expressionFactory.createSimpleDivision(4, 3);
        Assert.assertEquals(1, additionExpression.evaluate().intValue());
    }
}
