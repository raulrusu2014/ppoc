package com.tora.Model.Expression.Arithmetic;

import com.tora.Model.Expression.IExpression;

public class SubtractionExpression extends ArithmeticExpression{
    public SubtractionExpression(IExpression<Integer> firstTerm, IExpression<Integer> secondTerm){
        super(firstTerm, secondTerm);
    }

    @Override
    public Integer evaluate() {
        return firstTerm.evaluate() - secondTerm.evaluate();
    }
}
