package com.tora.Model.Expression.Simple;

import com.tora.Model.Expression.IExpression;

public class IntegerExpression implements IExpression<Integer> {
    private int value;

    public IntegerExpression(int value) {
        this.value = value;
    }

    @Override
    public Integer evaluate() {
        return value;
    }
}
