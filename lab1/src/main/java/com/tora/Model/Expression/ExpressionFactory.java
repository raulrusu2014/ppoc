package com.tora.Model.Expression;

import com.tora.Model.Expression.Arithmetic.AdditionExpression;
import com.tora.Model.Expression.Arithmetic.DivisionExpression;
import com.tora.Model.Expression.Arithmetic.MultiplicationExpression;
import com.tora.Model.Expression.Arithmetic.SubtractionExpression;
import com.tora.Model.Expression.Simple.IntegerExpression;

public class ExpressionFactory {
    public AdditionExpression createSimpleAddition(int firstTerm, int secondTerm) {
        return new AdditionExpression(new IntegerExpression(firstTerm), new IntegerExpression(secondTerm));
    }

    public AdditionExpression createAdditionExpression(IExpression<Integer>firstTerm, IExpression<Integer>secondTerm){
        return new AdditionExpression(firstTerm, secondTerm);
    }

    public SubtractionExpression createSimpleSubtraction(int firstTerm, int secondTerm) {
        return new SubtractionExpression(new IntegerExpression(firstTerm), new IntegerExpression(secondTerm));
    }

    public SubtractionExpression createSubtractionExpression(IExpression<Integer>firstTerm, IExpression<Integer>secondTerm){
        return new SubtractionExpression(firstTerm, secondTerm);
    }

    public MultiplicationExpression createSimpleMultiplication(int firstTerm, int secondTerm) {
        return new MultiplicationExpression(new IntegerExpression(firstTerm), new IntegerExpression(secondTerm));
    }

    public MultiplicationExpression createMultiplicationExpression(IExpression<Integer>firstTerm, IExpression<Integer>secondTerm){
        return new MultiplicationExpression(firstTerm, secondTerm);
    }

    public DivisionExpression createSimpleDivision(int firstTerm, int secondTerm) {
        return new DivisionExpression(new IntegerExpression(firstTerm), new IntegerExpression(secondTerm));
    }

    public DivisionExpression createAdditionDivision(IExpression<Integer>firstTerm, IExpression<Integer>secondTerm){
        return new DivisionExpression(firstTerm, secondTerm);
    }
}
