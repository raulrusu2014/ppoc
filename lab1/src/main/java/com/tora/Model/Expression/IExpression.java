package com.tora.Model.Expression;

public interface IExpression<T> {
    T evaluate();
}
