package com.tora.Model.Expression.Arithmetic;

import com.tora.Model.Expression.IExpression;

public abstract class ArithmeticExpression implements IExpression<Integer> {
    protected IExpression<Integer> firstTerm;
    protected IExpression<Integer> secondTerm;

    public ArithmeticExpression(IExpression<Integer> firstTerm, IExpression<Integer> secondTerm) {
        this.firstTerm = firstTerm;
        this.secondTerm = secondTerm;
    }
}
